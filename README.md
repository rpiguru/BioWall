# Working with BioWall devices

## Installing backend python script on RPi.

### Install dependencies.
    
    sudo pip install peewee
    sudo pip install modbus_tk

### Setup devices and DB

- If your Modbus device is connected to the other port, please open `config.xml` and change the `MODBUS_PORT` value.

- If you need to change the SQlite DB file, open the `config.xml` and change the `DB_NAME` value.

### Setup script execution guide.

- If a new sensor is attached besides of `PH`, `EC`, `Pressure`, `Moisture`, `Flow`, `Leak`, `Level`, `Temperature`, open the 
`config.xml` and add new sensor name to the `SENSORS` tag, and re-configure *Sensor* table.

- If a new output is attached beside of `Solenoid`, `Fertilizer`, `Light`, open the `config.xml` and add it to the 
 `OUTPUTS` local variable, and re-configure *Output* table.
 
- 


# Development guide

## Install Nodejs 0.12.X

    sudo apt-get install npm
    sudo npm install -g npm
    sudo npm cache clean -f
    sudo npm install -g n
    sudo n 0.12.4


## To develop on local server:

- Open the `webpack.config.js` and you could see something about `api_base_url`.

As a result, we have to set `API_BASE_URL` on the line 37 as `https://biowall.apps.exosite.io` - our solution url.

i.e. : 
    
    API_BASE_URL: JSON.stringify('https://biowall.apps.exosite.io'),

- Open the `http://localhost:8080` and it should work.

 

    
    
    