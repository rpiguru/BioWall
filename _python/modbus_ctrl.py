import os
import random
import threading
import traceback
import modbus_tk
import serial
from modbus_tk import modbus_rtu
import modbus_tk.defines as cst
from base import Base

import logging.config

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

try:
    logging.config.fileConfig(cur_dir + "logging.conf")
except IOError:
    print 'Failed to load configuration of logging'

logger = logging.getLogger("modbus")


class ModbusCtrl(Base):
    ctrl = None
    lock = threading.RLock()

    def __init__(self, port='/dev/rfxcom', baudrate=19200, byte_size=8, parity='N', stop_bits=1, xonxoff=0,
                 timeout=5.0, verbose=True):
        Base.__init__(self)
        try:
            self.ctrl = modbus_rtu.RtuMaster(serial.Serial(port=port, baudrate=baudrate, bytesize=byte_size,
                                                           parity=parity, stopbits=stop_bits,
                                                           xonxoff=xonxoff))
            self.ctrl.set_timeout(timeout)
            self.ctrl.set_verbose(verbose)
        except serial.SerialException as e:
            traceback.print_exc()
            logger.error(e)
            self.ctrl = None

    def read_holding_register(self, addr):
        if self.debug:
            return random.randint(1, 1000)

        if self.ctrl is None:
            return None

        # TODO: Add more comprehensive exception code since modbus_tk is not so stable(I feel so...)
        with self.lock:
            try:
                val = self.ctrl.execute(1, cst.READ_HOLDING_REGISTERS, addr, 1)[0]
                return val
            except modbus_tk.modbus.ModbusInvalidResponseError as e:
                logger.error(e)
                print e
                return None
            except OSError as e:
                logger.error(e)
                print e
                return None
            except:
                traceback.print_exc()
                logger.error(traceback.format_exc())
                return None

    def write_holding_register(self, addr, value):
        if self.ctrl is None:
            return False

        # TODO: Add more comprehensive exception code since modbus_tk is not so stable(I feel so...)
        with self.lock:
            try:
                # TODO: get return value of this function and see it is executed correctly...
                #  Not sure why the response does not match...
                #  Though I set register 101 as 1, its response was always (101, 0)
                self.ctrl.execute(1, cst.WRITE_SINGLE_REGISTER, addr, output_value=value)
                # print 'Return value of writing command: ', return_val
                return True
            except modbus_tk.modbus.ModbusInvalidResponseError as e:
                logger.error(e)
                print e
                return False
            except OSError as e:
                logger.error(e)
                print e
                return False
            except:
                traceback.print_exc()
                logger.error(traceback.format_exc())
                return False


if __name__ == '__main__':
    a = ModbusCtrl()
    print a.read_holding_register(197)
