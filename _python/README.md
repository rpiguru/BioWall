## Python backend script for biowall devices
============================================

### Execute backend script

Clone our repository and start backend script with
    
    cd ~
    git clone https://bitbucket.org/rpi_guru/biowall.git
    cd biowall/_python
    mkdir logs
    python biowall.py
    
It will check the local sqlite db and start setup script if the db is not configured.

After configuring setup, we have to make it run automatically at the booting time.

### Add detailed log
    
Besides of logging to the local log file, let us use `supervisor` to enable auto starting and system level detailed log.

    sudo cp /home/pi/biowall/_python/supervisord.conf /etc/supervisor/supervisord.conf
    
    sudo supervisord -c /etc/supervisor/supervisord.conf
    sudo supervisorctl -c /etc/supervisor/supervisord.conf
    sudo reboot
