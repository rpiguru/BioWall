import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import LinearProgress from 'material-ui/LinearProgress';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { white } from 'material-ui/styles/colors';
import React from 'react';
import { Link } from 'react-router';
import MessageBox from './MessageBox';

const Login = ({ errorText, isAuthenticating, infoText, onSubmit, handleOpenDialog }) => (
  <div className="auth-page">
    <AppBar
      iconElementRight={
        <RaisedButton
          containerElement={<Link to="/signup" />}
          label="Sign up"
          secondary
        />
      }
      showMenuIconButton={false}
      style={{ background: white, boxShadow: 'none' }}
    />

    <main className="container container--small">
      <div className="logo-container">
        <img src="images/Furbish-Logo.png" />
        {/*<h1>FURBISH</h1>*/}
        <br/>
        <h4>BioWall: Vegetated Wall | Quality Indoor Air</h4>
      </div>

      <h2 style={{ textAlign: 'center' }}>Login</h2>
      {errorText && <MessageBox error text={errorText} />}
      {infoText && <MessageBox info text={infoText} />}

      <form onSubmit={onSubmit}>
        <TextField
          autoFocus
          hintText="Email address"
          floatingLabelText="Email address"
          fullWidth
          name="email"
          underlineStyle={{borderColor: '#95A105'}}
                    underlineFocusStyle={{borderColor: '#95A105' }}
                    hintStyle={{color: '#95A105'}}
                    floatingLabelFocusStyle={{color: '#95A105'}}
          required
          type="email"
        />
        <TextField
          hintText="Password"
          floatingLabelText="Password"
          fullWidth
          underlineStyle={{borderColor: '#95A105'}}
                    underlineFocusStyle={{borderColor: '#95A105' }}
                    hintStyle={{color: '#95A105'}}
                    floatingLabelFocusStyle={{color: '#95A105'}}
          name="password"
          required
          type="password"
        />
        <RaisedButton
          disabled={isAuthenticating}
          fullWidth
          label="Login"
          backgroundColor={'#95A105'}
          style={{ marginTop: 16, width: '100%' }}
          type="submit"
        >
          {isAuthenticating && <LinearProgress color="#95A105"/>}

        </RaisedButton>
      </form>

      <FlatButton label="Forgot password?"
                  style={{ width: '100%', }}
                  onTouchTap={handleOpenDialog}
                  disabled={isAuthenticating}
      />
    </main>
    <footer className="version">
      Version 1.0.0
    </footer>
  </div>
);

Login.propTypes = {
  errorText: React.PropTypes.string,
  infoText: React.PropTypes.string,
  isAuthenticating: React.PropTypes.bool,
  onSubmit: React.PropTypes.func.isRequired,
  handleOpenDialog: React.PropTypes.func,
};

export default Login;
