import auth from './auth';
import biowalls from './biowalls';
import service from './service';
import session from './session';
import admin from './admin';

export default {
  ...auth,
  ...biowalls,
  service,
  ...session,
  ...admin,
};
