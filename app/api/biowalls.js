import store from '../store';
import service from './service';

function getBioWalls() {
  const { email } = store;
  return service.get(`/user/${email}/biowalls`);
}

function addBioWall(name, serialnumber) {
  const { email } = store;
  const body = {
    serialnumber,
    name
  };
  // console.log('Adding Biowall device, name: ' + name + ', sn: ' + serialnumber);
  return service.post(`/user/${email}/biowalls`, body);
}

function setBioWallState(serialNumber, state) {
  return service.post(`/biowall/${serialNumber}`, { state: state.toString() });
}

function getBioWallData(sn, alias, limit, alias_type, down_sample, start_time, end_time) {
  const { email } = store;
  const body = {
    alias,
    limit,
    alias_type,
    down_sample,
    start_time,
    end_time,
  };
  return service.post(`/user/${email}/get_biowall/${sn}`, body);
}

function updateBioWallData(sn, alias, action, value) {
  const { email } = store;
  const body = {
    alias,
    action,
    value
  };
  return service.post(`/user/${email}/update_biowall/${sn}`, body);
}

function updateBioWallName(serialnumber, biowall_name) {
  const { email } = store;
  const body = {
    serialnumber,
    biowall_name
  };
  return service.post(`/user/${email}/biowall`, body);
}

function removeBioWall(sn) {
  const { email } = store;
  return service.get(`/user/${email}/delete_biowall/${sn}`);
}

function get_alert(alert_type) {
  const {email} = store;
  return service.get(`/user/${email}/alert/${alert_type}`);
}

function add_alert(alert_type, alert_value){
  const {email} = store;
  const body = { alert_value};
  return service.post(`/user/${email}/add_alert/${alert_type}`, body);
}

function delete_alert(alert_type, alert_value){
  const {email} = store;
  const body = { alert_value};
  return service.post(`/user/${email}/delete_alert/${alert_type}`, body);
}


function dismissAlert(serialnumber) {
  const { email } = store;
  const body = {
    serialnumber
  };
  return service.post(`/user/${email}/dismiss_alert`, body);
}

export default {
  addBioWall,
  getBioWalls,
  setBioWallState,
  getBioWallData,
  updateBioWallData,
  removeBioWall,
  get_alert,
  add_alert,
  dismissAlert,
  delete_alert,
  updateBioWallName
};
